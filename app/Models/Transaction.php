<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Transaction extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'transaction_id',
        'tipe',
        'amount',
        'keterangan',
        'file',
    ];

    public function generate_transaction_id() {
        $dt = Carbon::now();
        $last_id = Transaction::max('id');
        $last_id++;
        return "T" . $dt->format('Ymd') . sprintf('%04d', $last_id);
    }
}
