<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Models\Transaction;
use App\Models\Saldo;

use Inertia\Inertia;
use Inertia\Response;


class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Transaction/Create', [
            'status' => session('status'),
            'error' => session('error'),
        ]);
    }
    
    public function history(Request $request)
    {
        $saldo = Saldo::where('user_id', auth()->user()->id)->first();
        $transaction = Transaction::where('user_id', auth()->user()->id);
        if($request->tipe) {
            $transaction = $transaction->where('tipe', $request->tipe);
        }
        if($request->search) {
            $transaction = $transaction->where('transaction_id', 'like', '%' . $request->search . '%')
                ->orWhere('tipe', 'like', '%' . $request->search . '%')
                ->orWhere('amount', 'like', '%' . $request->search . '%')
                ->orWhere('keterangan', 'like', '%' . $request->search . '%');
        }
        $transaction = $transaction->orderBy('id','desc')->paginate(3);
        foreach ($transaction as $value) {
           $value->file = Storage::disk('public')->url($value->file);
        }
        return Inertia::render('Transaction/History', [
            'status' => session('status'),
            'saldo' => $saldo->saldo,
            'transaction' => $transaction,
            'tipe' => $request->get('tipe'),
            'search' => $request->get('search'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // cek saldo
        $saldo = Saldo::firstOrCreate(['user_id' => $request->user_id], ['saldo' => 0]);
        if ($request->tipe === "transaction") {
            $saldo->saldo -= $request->amount;
            if ($request->amount > $saldo->saldo) {
                return redirect()->route('transaction.index')->with('error','Saldo tidak mencukupi');
            }
        } elseif ($request->tipe === "top_up") {
            $saldo->saldo += $request->amount;
        }


        // store transaction
        $transaction = new Transaction;
        $transaction->user_id = $request->user_id;
        $transaction->transaction_id = $transaction->generate_transaction_id();
        $transaction->tipe = $request->tipe;
        $transaction->amount = $request->amount;
        $transaction->keterangan = $request->keterangan;
        
        // store file
        $filename = $transaction->transaction_id.".".$request->file('file')->extension();
        $path = Storage::disk('public')->putFileAs('files', $request->file('file'), $filename, 'public');
        $transaction->file = $path;
        $transaction->save();

        // store saldo
        $saldo->save();

        return redirect()->route('transaction.success', [
            'status' => session('status'),
            'id' => $transaction->id,
        ]);
    }
    
    public function success(Request $request)
    {
        return Inertia::render('Transaction/Success', [
            'status' => session('status'),
            'tipe' => $request->get('tipe'),
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
