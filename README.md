# Yukk Full Stack Test Kamal Firdaus



## Installation

- copy .env.example to .env
- docker-compose up -d (if not using docker compose then install postgres in local)
- create database with name laravel_yukk
- composer install
- php artisan key:generate
- php artisan migrate
- php artisan storage:link
- php artisan serve --port=80
- add this line to your windows C:\Windows\System32\drivers\etc\hosts (or similiar to hosts)
```
127.0.0.1	yukk.test
```
- create new terminal (different one from artisan serve)
- npm install
- npm run dev
- open browser url http://yukk.test/ or localhost:80