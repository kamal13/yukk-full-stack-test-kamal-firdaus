import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import CreateTransaction from './Partials/CreateTransaction';
import TabTransaction from './Partials/TabTransaction';
import { Head } from '@inertiajs/react';

export default function Create({ auth, status, error }) {
    return (
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Transaction</h2>}
        >
            <Head title="Transaction" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
                    <div className="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                        <TabTransaction />
                        
                        {error && (
                            <div className="p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800" role="alert">{error}</div>
                        )}
                        <CreateTransaction
                            status={status}
                            className="max-w-xl"
                            user_id={auth.user.id}
                        />
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
