import { useRef } from 'react';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { useForm } from '@inertiajs/react';
import { Transition } from '@headlessui/react';

export default function CreateTransaction({ className = '', user_id = '' }) {
    const tipeInput = useRef();

    const { data, setData, errors, post, reset, processing, recentlySuccessful } = useForm({
        user_id: user_id,
        tipe: '',
        amount: '',
        keterangan: '',
        file: '',
    });

    const handleChangeTipe = (e) => {
        setData('tipe', e.target.value);
        data.file = '';
    };
    const handleChangeFile = (e) => {
        setData('file', e.target.files[0]);
    };

    const createTransaction = (e) => {
        e.preventDefault();

        post(route('transaction.create'), {
            preserveScroll: true,
            onSuccess: () => reset(),
            onError: (errors) => {
                if (errors.tipe) {
                    reset('tipe');
                    tipeInput.current.focus();
                }
            },
        });
    };

    return (
        <section className={className}>

            <h2 className="text-lg font-medium text-gray-900">Create Transaction</h2>

            <form onSubmit={createTransaction} className="mt-6 space-y-6">
                <div>
                    <InputLabel htmlFor="tipe" value="Tipe" />

                    <div className="radio block font-medium text-sm text-gray-700">
                        <label className="mr-5">
                            <input type="radio" className='mr-2' value="top_up" checked={data.tipe === 'top_up'} onChange={handleChangeTipe} />Top Up
                        </label>
                        <label>
                            <input type="radio" className='mr-2' value="transaction" checked={data.tipe === 'transaction'} onChange={handleChangeTipe} />Transaction
                        </label>
                    </div>
                    
                    <InputError message={errors.tipe} className="mt-2" />
                </div>

                <div>
                    <InputLabel htmlFor="amount" value="Amount" />

                    <TextInput
                        id="amount"
                        value={data.amount}
                        onChange={(e) => setData('amount', e.target.value)}
                        type="number"
                        className="mt-1 block w-full"
                        required={true}
                    />

                    <InputError message={errors.amount} className="mt-2" />
                </div>

                <div>
                    <InputLabel htmlFor="keterangan" value="Keterangan" />

                    <TextInput
                        id="keterangan"
                        value={data.keterangan}
                        onChange={(e) => setData('keterangan', e.target.value)}
                        type="text"
                        className="mt-1 block w-full"
                        required={true}
                    />

                    <InputError message={errors.keterangan} className="mt-2" />
                </div>
                
                { data.tipe === 'top_up' &&
                    <div>
                        <InputLabel htmlFor="file" value="File" />

                        <TextInput
                            id="file"
                            onChange={handleChangeFile}
                            type="file"
                            accept="image/*"
                            className="mt-1 block w-full"
                            required={true}
                        />

                        <InputError message={errors.file} className="mt-2" />
                    </div>
                }

                <div className="flex items-center gap-4">
                    <PrimaryButton disabled={processing}>Save</PrimaryButton>

                    <Transition
                        show={recentlySuccessful}
                        enter="transition ease-in-out"
                        enterFrom="opacity-0"
                        leave="transition ease-in-out"
                        leaveTo="opacity-0"
                    >
                        <p className="text-sm text-gray-600">Saved.</p>
                    </Transition>
                </div>
            </form>
        </section>
    );
}
