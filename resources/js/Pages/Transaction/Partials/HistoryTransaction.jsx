import { useForm } from '@inertiajs/react';
import { Pagination, Table, Button } from 'flowbite-react';
import TextInput from '@/Components/TextInput';

export default function HistoryTransaction({ className = '', saldo = '', transaction = [], tipe = '', search = '' }) {

    const { data, setData, errors, post, reset, processing, recentlySuccessful } = useForm({
        search: search,
        currentPage: '',
    });

    const onPageChange = (page) => {
        let location = transaction.path + '?page=' + page
        if(tipe) { location += '&tipe=' + tipe }
        window.location = location
    };
    const onFilterTopup = () => {window.location = transaction.path + '?tipe=top_up'};
    const onFilterTransaction = () => {window.location = transaction.path + '?tipe=transaction'};
    const onFilterAll = () => {window.location = transaction.path};
    const onSearch = (e) => {
        if(e.key === 'Enter'){
            let location = transaction.path + '?page=1'
            if(tipe) { location += '&tipe=' + tipe }
            if(data.search) { location += '&search=' + data.search }
            window.location = location;
        }
    }

    return (
        <section className={className}>
            <section className='shadow sm:rounded-lg p-5 mb-5 bg-blue-200'>
                <h1 className="text-lg font-medium text-gray-900">Current Balance</h1>
                <h3 className="text-3xl font-bold text-gray-900">{Number(saldo).toLocaleString()}</h3>
            </section>

            <h2 className="text-lg font-medium text-gray-900">History Transaction</h2>

            <div className="mt-6 space-y-6">
                <div className="overflow-x-auto">
                    <div className='mb-4 flex'>
                        <div className='flex-1 mr-5'>
                            <TextInput
                                id="search"
                                value={data.search}
                                onChange={(e) => setData('search', e.target.value)}
                                onKeyDown={onSearch}
                                type="text"
                                className="w-full block"
                                placeholder="Search..."
                            />
                        </div>

                        <div>
                            <Button.Group>
                                <Button color={tipe === 'top_up' ? 'blue' : 'gray'} onClick={onFilterTopup}>Top Up</Button>
                                <Button color={tipe === 'transaction' ? 'blue' : 'gray'} onClick={onFilterTransaction}>Transaction</Button>
                                <Button color={tipe === null ? 'blue' : 'gray'} onClick={onFilterAll}>All</Button>
                            </Button.Group>
                        </div>
                    </div>
                    <Table>
                        <Table.Head>
                            <Table.HeadCell>Date</Table.HeadCell>
                            <Table.HeadCell>ID</Table.HeadCell>
                            <Table.HeadCell>Tipe</Table.HeadCell>
                            <Table.HeadCell>Amount</Table.HeadCell>
                            <Table.HeadCell>Keterangan</Table.HeadCell>
                            <Table.HeadCell>
                                <span className="sr-only">File</span>
                            </Table.HeadCell>
                        </Table.Head>
                        <Table.Body className="divide-y">
                            {transaction.data.map((item) => (
                            <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800" key={item.id}>
                                <Table.Cell>{new Date(item.created_at).toLocaleString()}</Table.Cell>
                                <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                {item.transaction_id}
                                </Table.Cell>
                                <Table.Cell>{item.tipe}</Table.Cell>
                                <Table.Cell className='text-right'>{item.tipe === 'top_up' ? '+' : '-'} {Number(item.amount).toLocaleString()}</Table.Cell>
                                <Table.Cell>{item.keterangan}</Table.Cell>
                                <Table.Cell>
                                {item.file && (
                                    <a href={item.file} target="_blank" className="font-medium text-cyan-600 hover:underline dark:text-cyan-500">
                                        File
                                    </a>
                                )}
                                </Table.Cell>
                            </Table.Row>
                            ))}
                        </Table.Body>
                    </Table>
                    </div>

                <div className="flex overflow-x-auto sm:justify-center">
                    <Pagination currentPage={transaction.current_page} totalPages={transaction.last_page} onPageChange={onPageChange} />
                </div>
            </div>
        </section>
    );
}
