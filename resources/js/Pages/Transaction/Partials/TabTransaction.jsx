
export default function TabTransaction({ auth, status }) {
    const activeClass = 'inline-block p-4 text-blue-600 border-b-2 border-blue-600 rounded-t-lg active dark:text-blue-500 dark:border-blue-500'
    const inactiveClass = 'inline-block p-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300'
    return (
        <div className="text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700 mb-7">
            <ul className="flex flex-wrap -mb-px">
                <li className="me-2">
                    <a href={route('transaction.index')} className={(route().current('transaction.index')) ? activeClass : inactiveClass}>Transaction</a>
                </li>
                <li className="me-2">
                    <a href={route('transaction.history')} className={(route().current('transaction.history')) ? activeClass : inactiveClass}>History</a>
                </li>
            </ul>
        </div>
    );
}
