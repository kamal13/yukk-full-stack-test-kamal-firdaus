import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import HistoryTransaction from './Partials/HistoryTransaction';
import TabTransaction from './Partials/TabTransaction';
import { Head } from '@inertiajs/react';

export default function Create({ auth, status, saldo, transaction, tipe, search }) {
    return (
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Transaction</h2>}
        >
            <Head title="Transaction" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
                    <div className="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                        <TabTransaction />
                        <HistoryTransaction
                            status={status}
                            saldo={saldo}
                            transaction={transaction}
                            tipe={tipe}
                            search={search}
                            className="max-w-3xl"
                        />
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
